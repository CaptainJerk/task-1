/**
 * @author      : captain (captain@arch)
 * @file        : main
 * @created     : Feb 08, 2020
 */

#ifndef MAIN_HPP
#define MAIN_HPP

#include <fstream>
#include <chrono>
#include <cmath>
#include <iostream>
#include "cxxopts.hpp"
#include "kfr/base.hpp"
#include "kfr/dsp.hpp"
#include "libmodbuswrapper.hpp"

class App
{
    public:
        bool update();
        App(int, char**);
        ~App();
    private:
        /* Some default values */
        const std::string DEF_BAUD      = "38400";
        const std::string DEF_PARITY    = "E";
        const std::string DEF_UNIT      = "3";
        const std::string DEF_DATABITS  = "8";
        const std::string DEF_STOPBITS  = "2";
        const std::string DEF_FS        = "5.0";
        const std::string DEF_CUTOFF    = "0.5";
        const std::string DEF_TRF       = "0.01";
        const std::string DEF_ATTEN     = "31";
        const std::string DEF_NOISE     = "6";
        const std::string DEF_SIGNAL    = "5";
        const std::string DEF_SEP       = ";";

        /* Writes application result */
        void write_output(double, double);
        /* Check if sampling request is pending */
        bool sampling_request(void);
        /* Read current signal and noise values */
        bool read_out(void);

        /* Register offsets */
        int noise_reg;
        int signal_reg;

        /* Last sample of signal/noise */
        double noise = .0;
        double signal = .0;
        
        /* Sampling period. Used for triggering sampling request */
        float ts;
        /* CSV column separator */
        char sep;

        /* Filter. */
        kfr::univector<double, 31> taps;
        kfr::filter_fir<double> *filt;

        /* libmodbus wrapper instance */
        libmodbuswrapper *modbus;
        /* Sampling request timer and program start timepoint */
        std::chrono::time_point<std::chrono::high_resolution_clock> t0;
        std::chrono::time_point<std::chrono::high_resolution_clock> t_start;
};

#endif /* end of include guard MAIN_HPP */

