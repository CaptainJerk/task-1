/**
 * @author      : captain (captain@Captains-MacBook-Pro-2.local)
 * @file        : libmodbuswrapper
 * @created     : Feb 08, 2020
 * @description : Simple task-specific libmodbus wrapper for cpp.
 */

#ifndef LIBMODBUSWRAPPER_HPP
#define LIBMODBUSWRAPPER_HPP

#include <iostream>
#include <modbus.h>

class libmodbuswrapper
{
    public:
        libmodbuswrapper(std::string dev, int baud, char parity,
                                    int data_bit, int stop_bit);
        virtual ~libmodbuswrapper();
        int connect(void);
        void close(void);
        int read_input_registers(int addr, int nb, uint16_t *dest);
        float get_float(uint16_t *src);
        int set_slave(int slave);
        int baud;
        int databits;
        int stopbits;
        char parity;
        int unit;
        std::string dev;

    private:
        /* libmodbus rtu instance */
        modbus_t *inst;
};

#endif /* end of include guard LIBMODBUSWRAPPER_HPP */

