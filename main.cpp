/**
 * @author      : captain (captain@arch)
 * @file        : main
 * @created     : Feb 08, 2020
 */

#include "main.hpp"

using namespace std;
using namespace cxxopts;
using namespace kfr;

bool App::update(void)
{
    /* To keep user interface responsive, this method should be 
     * reimplemented with requests issued in asynchronous manner 
     * (thread pool + callback chain), but for a simple terminal app 
     * this should do well.
     * The slight jitter introduced by the clock instability/cache refill/bus delays/etc 
     * will affect the filter cutoff frequency. But since signal and noise
     * are relatively low frequency, I will skip the resampling stage
     * and assume the sampling frequency constant.
     */
    if (sampling_request()) {
        /* As per the task: */
        /* 1. Read out signal and noise */
        if (read_out()) {
            /* 2. Add up signal and noise */
            double raw = signal + noise;
            /* 3. Filter the mixed signal */
            double filtered;
            filt->apply(&filtered, &raw, 1);
            /* 4. Append the data into a CSV file */
            write_output(raw, filtered);
        }
        else {
            cerr << "Unable to read the data. I quit." << endl;
            return false;
        }
    }
    return true; // Loop until SIGINT/SIGTERM
}

bool App::sampling_request(void)
{
    /* The method yields true when it is time to sample the data. */
    auto t = chrono::high_resolution_clock::now();
    if (chrono::duration_cast<chrono::milliseconds>(t - t0).count() >= ts) {
        t0 = t;
        return true;
    }
    return false;
}

bool App::read_out(void)
{
    uint16_t _signal[1];
    uint16_t _noise[2];
    int ret = modbus->read_input_registers(noise_reg, 2, _noise);
    ret += modbus->read_input_registers(signal_reg, 1, _signal);
    /* Convert noise registers into float */
    noise = modbus->get_float(_noise);
    /* Signal is a single word register */
    signal = _signal[0];
    return ret == 3;
}

void App::write_output(double raw, double filtered)
{
    cout << chrono::duration_cast<chrono::milliseconds>(t0 - t_start).count() << \
        sep << raw << sep << filtered << endl;
}

App::App(int argc, char *argv[])
{
    Options o(argv[0],  "The data is sent to stdout. To redirect into file: " + \
                        string(argv[0]) + " 1>out.csv\n" \
                        "While the application is running - press Ctrl+c to exit.");
    auto _ = o.add_options();
    _("h,help",     "Display the message");
    /* Bus sampling rate */
    _("f,fs",       "Bus sampling rate, Hz",                    value<float>()->default_value(DEF_FS));
    _("b,baud",     "Serial baud rate",                         value<int>()->default_value(DEF_BAUD));
    _("p,parity",   "Serial parity (O[dd], E[ven], N[one])",    value<char>()->default_value(DEF_PARITY));
    _("d,databits", "Number of serial data bits",               value<int>()->default_value(DEF_DATABITS));
    _("s,stopbits", "Number of serial stop bits",               value<int>()->default_value(DEF_STOPBITS));
    _("u,unit",     "Modbus unit address",                      value<int>()->default_value(DEF_UNIT));
    _("n,noise",    "Noise register offset",                    value<int>()->default_value(DEF_NOISE));
    _("v,signal",   "Signal register offset",                   value<int>()->default_value(DEF_SIGNAL));
    _("c,cutoff",   "Noise filter cutoff frequency, Hz",        value<float>()->default_value(DEF_CUTOFF));
    _("x,separator","CSV column separator",                     value<char>()->default_value(DEF_SEP));
    _("dev",        "Serial device",                            value<string>());
    o.parse_positional("dev");
    auto opts = o.parse(argc, argv);
    if (opts.count("help")) {
        /* Help request. Display and exit. */
        cerr << o.help() << endl;
        exit(0);
    }
    if (!opts.count("dev")) {
        /* You only had to specify a single parameter and you failed... */
        cerr << "Please define a serial device (see " << argv[0] << " -h)." << endl;
        exit(1);
    }

    /* Constrain filter requirements data */
    float _fs = max(.1f, opts["fs"].as<float>());
    float _cutoff = min(opts["cutoff"].as<float>(), _fs / 2.01f);

    /* Build a filter */
    auto win = to_pointer(window_hann(taps.size()));
    fir_lowpass(taps, _cutoff / _fs, win, true);
    filt = new filter_fir<double>(taps);
    fstream filter_out("filter.csv", ios::out);
    for (int i = 0; i < taps.size(); i++) {
        filter_out << taps[i] << endl;
    }
    cerr << "Filter taps have been saved to filter.csv" << endl;
    filter_out.close();

    /* Registers to read */
    noise_reg = opts["noise"].as<int>();
    signal_reg = opts["signal"].as<int>();

    /* Bus setup */
    modbus = new libmodbuswrapper(  opts["dev"].as<string>(),
                                    opts["baud"].as<int>(),
                                    opts["parity"].as<char>(),
                                    opts["databits"].as<int>(),
                                    opts["stopbits"].as<int>() );
    modbus->connect();
    modbus->set_slave(opts["unit"].as<int>());

    /* CSV file separator */
    sep = opts["separator"].as<char>();

    /* Sampling timer */
    ts = 1000.f / _fs;
    t0 = std::chrono::high_resolution_clock::now();
    t_start = std::chrono::high_resolution_clock::now();
}

App::~App()
{
    modbus->close();
    delete modbus;
}

int main(int argc, char *argv[])
{
    App *a = new App(argc, argv);
    while (a->update());
    delete a;
    return 0;
}

