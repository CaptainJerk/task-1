#!/usr/bin/env python3

from pymodbus.server.sync import StartSerialServer
from pymodbus.datastore import (ModbusSlaveContext, 
                                ModbusServerContext, 
                                ModbusSparseDataBlock)
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.transaction import ModbusRtuFramer
import time
import math
import logging
import subprocess

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class CustomDataBlock(ModbusSparseDataBlock):
    def __init__(self):
        # Dummy
        self.__values = dict()
        # Initial time
        self.__t_start = time.time()
        self.__builder = BinaryPayloadBuilder(byteorder="<", wordorder="<")
        super().__init__(self.__values)

    @property
    def values(self):
        self.__builder.reset()
        self.__builder.add_16bit_uint(self.signal)
        self.__builder.add_32bit_float(self.noise)
        ret = {key: val for key, val in zip((5, 6, 7), self.__builder.to_registers())}
        return ret

    @values.setter
    def values(self, val):
        self.__values = val

    def _swap(self, val):
        # Byte order swap
        return int.from_bytes(val.to_bytes(2, byteorder="little"),
                                byteorder="big")

    @property
    def t(self):
        # Return time in seconds (with fractions) since the 
        # instance initialized
        return time.time() - self.__t_start
    
    @property
    def signal(self):
        return self._swap(5) 

    @property
    def noise(self):
        amp = 1.
        freq = 1. # Hz
        return amp * math.sin(2. * math.pi * freq * self.t)

def main():
    subprocess.Popen(["socat", "pty,link=vtty0,raw", "pty,link=vtty1,raw"])
    slave = ModbusSlaveContext(ir=CustomDataBlock(), zero_mode=True)
    server = ModbusServerContext(slaves={0x03: slave}, single=False)
    logger.info("The server is up and running, use port vtty1")
    StartSerialServer(server, timeout=.05, port="vtty0", baudrate=38400, parity="E", framer=ModbusRtuFramer)

if __name__ == "__main__":
    main()

