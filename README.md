# Описание
Сборка - под Linux/MacOS. Для Windows - придется повозиться с cygwin. По какой-то причине KFR не собирается GCC 9.2 - используем clang.
Сначала необходимо собрать и установить libmodbus:
```
wget https://libmodbus.org/releases/libmodbus-3.1.6.tar.gz
tar -xf libmodbus-3.1.6.tar.gz
cd libmodbus-3.1.6
./configure
make && make install
```
Затем - сам проект:
```
git clone --recursive https://...
cd task-1 && make build && cd build
CMAKE_CXX_COMPILER=clang++ cmake ..
make
```
Так как под Windows нужно много устанавливать и настраивать - я написал простой эмулятор modbus сервера (на Python 3), для запуска - выполнить:
```
pip3 install pymodbus
python3 server
```
Команда выведет номер название символьной ссылки (vtty1) - к ней можно подключить тестовую программу... или скомпилировать ее для Windows (сложнее), или пробросить последовательный порт на виртуальную машину с Windows.
В программе доступна краткая справка:
```
./build/task_1 -h
```
Все параметры должны работать и с исходным эмулятором modbus по умолчанию.
Результат выполнения:
```
./build/task_1 vtty1 1>out.csv
```
![Test run result](result.png)