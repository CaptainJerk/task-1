/**
 * @author      : captain (captain@Captains-MacBook-Pro-2.local)
 * @file        : libmodbuswrapper
 * @created     : Feb 08, 2020
 */

#include <cstdlib>
#include <iostream>
#include "libmodbuswrapper.hpp"

using namespace std;

float libmodbuswrapper::get_float(uint16_t *src)
{
    return modbus_get_float_abcd(src);
}

void libmodbuswrapper::close(void)
{
    modbus_close(inst);
    cerr << "Disconnected" << endl;
}

int libmodbuswrapper::connect(void)
{
    int ret = modbus_connect(inst);
    if (ret < 0) {
        cerr << string(modbus_strerror(errno)) << endl;
    }
    else {
        cerr << "Connected" << endl;
    }
    return ret;
}

int libmodbuswrapper::set_slave(int slave)
{
    cerr << "Unit is set to " << slave << endl;
    unit = slave;
    return modbus_set_slave(inst, slave);
}

int libmodbuswrapper::read_input_registers(int addr, int nb, uint16_t *dest)
{
    int ret = modbus_read_input_registers(inst, addr, nb, dest);
    if (ret < 0) {
        cerr << "IR request to " << unit << " at " << addr << \
            " failed: " << string(modbus_strerror(errno)) << endl;
    }
    return ret;
}

libmodbuswrapper::libmodbuswrapper(std::string dev, int baud, char parity,
                                    int data_bit, int stop_bit)
{
    this->baud = baud;
    this->dev = dev;
    this->parity = parity;
    this->databits = data_bit;
    this->stopbits = stop_bit;
    inst = modbus_new_rtu(dev.c_str(), baud, parity, data_bit, stop_bit);
    if (!inst) {
        cerr << string(modbus_strerror(errno)) << endl;
    }
}

libmodbuswrapper::~libmodbuswrapper()
{
    modbus_free(inst);
}

